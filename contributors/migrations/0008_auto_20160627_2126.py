# -*- coding: utf-8 -*-


from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('contributors', '0007_auto_20160625_1002'),
    ]

    operations = [
        migrations.AlterField(
            model_name='aggregatedperson',
            name='user',
            field=models.OneToOneField(related_name='aggregated_person', to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE),
        ),
        migrations.AlterField(
            model_name='aggregatedsource',
            name='source',
            field=models.OneToOneField(related_name='aggregated_source', to='contributors.Source', on_delete=models.CASCADE),
        ),
    ]
