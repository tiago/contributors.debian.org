from __future__ import annotations
from django.urls import path
from importer import views as iviews
from . import views

urlpatterns = [
    path('year/<int:year>', views.Contributors.as_view(), name="contributors_year"),
    path('post', iviews.PostSourceView.as_view(), name="contributors_post"),
    path('test_post', iviews.TestPostSourceView.as_view(), name="contributors_test_post"),
    path('flat', views.ContributorsFlat.as_view(), name="contributors_flat"),
    path('new', views.ContributorsNew.as_view(), name="contributors_new"),
    path('mia', views.ContributorsMIA.as_view(), name="contributors_mia"),
    path('mia/query', views.MIAQuery.as_view(), name="contributors_mia_query"),
    path('sources/flat', views.SourcesFlat.as_view(), name="contributors_sources_flat"),
    path('export/sources', iviews.ExportSources.as_view(), name="contributors_export_sources"),
    path('site_status', views.SiteStatus.as_view(), name="site_status"),
    path('test_messages', views.TestMessages.as_view(), name="test_messages"),
    path('export/public', iviews.ExportDB.as_view(), name="contributors_export_public"),
]
