from __future__ import annotations
from django.test import TestCase
from dc.unittest import SourceFixtureMixin, NamedObjects
from contributors.models import User, UserSourceSettings


class TestMerge(SourceFixtureMixin, TestCase):
    @classmethod
    def setUpClass(cls):
        from signon.models import Identity
        super().setUpClass()
        cls.add_named_objects(
            identities=NamedObjects(Identity),
        )
        cls.identities.create(
            "dd_debsso", person=cls.users.dd, issuer="debsso", subject="dd@debian.org", audit_skip=True)
        cls.identities.create(
            "dd_salsa", person=cls.users.dd, issuer="salsa", subject="0", audit_skip=True)
        cls.identities.create(
            "dd1_debsso", person=cls.users.dd1, issuer="debsso", subject="dd1@debian.org", audit_skip=True)
        cls.identities.create(
            "dd1_salsa", person=cls.users.dd1, issuer="salsa", subject="1", audit_skip=True)

    def test_merge(self):
        self.users.dd.merge(self.users.dd1, audit_author=self.users.admin, audit_notes="Merge dd1 into dd")

        # dd1 should have disappeared
        self.assertTrue(User.objects.filter(pk=self.users.dd.pk).exists())
        self.assertFalse(User.objects.filter(pk=self.users.dd1.pk).exists())

        # User settings should be preserved
        self.users.dd.refresh_from_db()
        self.assertEqual(self.users.dd.username, "dd@debian")
        self.assertEqual(self.users.dd.full_name, "dd")
        self.assertTrue(self.users.dd.is_dd)
        self.assertFalse(self.users.dd.is_staff)
        self.assertFalse(self.users.dd.hidden)

        # Contributor identities should have migrated
        self.idents.refresh()
        self.assertEqual(self.idents.dd_user.user, self.users.dd)
        self.assertEqual(self.idents.dd_home.user, self.users.dd)
        self.assertEqual(self.idents.dd1_user.user, self.users.dd)
        self.assertEqual(self.idents.dd1_home.user, self.users.dd)

        # Signon identities should have migrated
        self.identities.refresh()
        self.assertEqual(self.identities.dd_debsso.person, self.users.dd)
        self.assertEqual(self.identities.dd_salsa.person, self.users.dd)
        self.assertEqual(self.identities.dd1_debsso.person, self.users.dd)
        self.assertEqual(self.identities.dd1_salsa.person, self.users.dd)

    def test_merge_hidden_user(self):
        self.users.dd1.hidden = True
        self.users.dd1.save()

        self.users.dd.merge(self.users.dd1, audit_author=self.users.admin, audit_notes="Merge dd1 into dd")

        self.users.dd.refresh_from_db()
        self.idents.refresh()

        # User should still be visible, but the identities that were associated
        # with the hidden user that was merged, should now be hidden
        self.assertFalse(self.users.dd.hidden)
        self.assertTrue(self.idents.dd1_user.hidden)
        self.assertTrue(self.idents.dd1_home.hidden)

    def test_merge_settings(self):
        dd_settings = self.sources.test.get_settings(self.users.dd, create=True)
        dd_settings.hidden = False
        dd_settings.save()

        dd1_settings = self.sources.test.get_settings(self.users.dd1, create=True)
        dd1_settings.hidden = True
        dd1_settings.save()

        self.users.dd.merge(self.users.dd1, audit_author=self.users.admin, audit_notes="Merge dd1 into dd")

        self.users.dd.refresh_from_db()
        self.idents.refresh()

        # Visibility of users and identities should not have changed
        self.assertFalse(self.users.dd.hidden)
        self.assertFalse(self.idents.dd_user.hidden)
        self.assertFalse(self.idents.dd_home.hidden)
        self.assertFalse(self.idents.dd1_user.hidden)
        self.assertFalse(self.idents.dd1_home.hidden)

        # Settings should have gotten merged conservatively
        self.assertFalse(UserSourceSettings.objects.filter(pk=dd1_settings.pk).exists())
        dd_settings.refresh_from_db()
        self.assertTrue(dd_settings.hidden)
