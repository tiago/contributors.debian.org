from __future__ import annotations
from django.utils.translation import gettext_lazy as _
from django import forms
from dc.lib.forms import BootstrapAttrsMixin
import contributors.models as cmodels


class UsernameForm(BootstrapAttrsMixin, forms.Form):
    username = forms.CharField(
            label=_("User name"), required=True,
            help_text=_("Enter the user name to which you are going to associate identifiers."
                        " It defaults to your user name."))

    def clean_username(self):
        data = self.cleaned_data["username"]
        if cmodels.User.objects.filter(username=data).exists():
            return data

        raise forms.ValidationError(
                _("Not a valid user name"), code="invalid")


class EmailIdentityForm(BootstrapAttrsMixin, forms.Form):
    email = forms.CharField(
            label=_("E-Mail"), required=True,
            help_text=_("Enter the email to associate to the user"))

    def __init__(self, *args, **kw):
        super().__init__(*args, **kw)
        self.fields["email"].widget.attrs["placeholder"] = "user@example.org"

    def clean_email(self):
        data = self.cleaned_data["email"]
        if cmodels.Identifier.objects.filter(type="email", name=data).exists():
            return data

        raise forms.ValidationError(
                _("This email is not known to the site"), code="invalid")


class FingerprintIdentityForm(BootstrapAttrsMixin, forms.Form):
    fpr = forms.CharField(
            label=_("GPG key fingerprint"), required=True,
            help_text=_("Enter the fingerprint to associate to the user"))

    def __init__(self, *args, **kw):
        super().__init__(*args, **kw)
        self.fields["fpr"].widget.attrs["placeholder"] = "1234 5678 9ABC DEF0 1234  5678 9ABC DEF0 1234 5678"

    def clean_fpr(self):
        data = self.cleaned_data["fpr"].replace(' ', '')
        if cmodels.Identifier.objects.filter(type="fpr", name=data).exists():
            return data

        raise forms.ValidationError(
                _("This fingerprint is not known to the site"), code="invalid")


class LoginIdentityForm(BootstrapAttrsMixin, forms.Form):
    login = forms.CharField(
            label=_("Login name"), required=True,
            help_text=_("Enter the LDAP username to associate to the user"))

    def __init__(self, *args, **kw):
        super().__init__(*args, **kw)
        self.fields["login"].widget.attrs["placeholder"] = "username"

    def clean_login(self):
        data = self.cleaned_data["login"]
        if cmodels.Identifier.objects.filter(type="login", name=data).exists():
            return data

        raise forms.ValidationError(
                _("This login is not known to the site"), code="invalid")
