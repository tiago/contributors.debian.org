from contributors import models
from debiancontributors import parser
from django.utils import timezone
from django.conf import settings
import django.http
from requests.utils import requote_uri
from .models import Submission, SubmissionLog
from .batch import BatchImport
from importer import serializers
import os
import logging

log = logging.getLogger(__name__)


class Importer:
    """
    Data import infrastructure

    Methods starting with import_* return True if something was imported, else
    False.

    Other methods return what they should, or raise Fail if some validation failed.
    """
    @classmethod
    def import_pending_trigger_file(cls):
        return os.path.join(settings.DATA_DIR, "trigger", "import_pending")

    @classmethod
    def clear_pending_trigger_file(cls):
        try:
            os.unlink(cls.import_pending_trigger_file())
        except FileNotFoundError:
            pass

    @classmethod
    def create_pending_trigger_file(cls):
        """
        Create the trigger file that notifies that imports are pending
        """
        os.makedirs(os.path.join(settings.DATA_DIR, "trigger"), exist_ok=True)
        with open(cls.import_pending_trigger_file(), "wb"):
            pass

    def enqueue_request(self, http_request: django.http.HttpRequest) -> SubmissionLog:
        submission, log = Submission.from_request(http_request, verify_auth_token=True)
        if log.result_code != 200:
            submission.completed = timezone.now().astimezone(timezone.utc)
        submission.save()
        log.submission = submission
        log.save()
        if log.result_code == 200:
            self.create_pending_trigger_file()
        return log

    def test_enqueue_request(self, http_request: django.http.HttpRequest) -> SubmissionLog:
        submission, log = Submission.from_request(http_request, verify_auth_token=False)
        return log

    def import_submission(self, submission) -> SubmissionLog:
        with submission.track("import") as (log, stats):
            Engine = BatchImport.by_name(submission.method)
            import_engine = Engine(submission.source, stats, submission.get_contributions())
            import_engine.run()

        if log.result_code == 200:
            # Update the health indicators
            submission.source.last_import = timezone.now()
            if import_engine.latest_entry is not None:
                submission.source.last_contribution = import_engine.latest_entry
            submission.source.save()
            # Reaggregate only AggregatedSource and AggregatedPerson, as the
            # Engine took care of updating AggregatedPersonContribution
            models.AggregatedSource.recompute(source=submission.source)
            models.AggregatedPerson.recompute()

        submission.completed = timezone.now().astimezone(timezone.utc)
        submission.save()

        log.save()
        return log

    def import_request(self, http_request: django.http.HttpRequest) -> django.http.HttpResponse:
        """
        Validate and import data from a request
        """
        receive_log = self.enqueue_request(http_request)
        if receive_log.result_code != 200:
            return receive_log.to_response()

        import_log = self.import_submission(receive_log.submission)
        return import_log.to_response()


class SourceImporter:
    """
    Source description import infrastructure

    Methods starting with import_* return True if something was imported, else
    False.

    Other methods return what they should, or raise Fail if some validation failed.
    """
    def get_contribution_type(self, source, data):
        if not isinstance(data, dict):
            raise parser.Fail(400, "contribution type data is not an object")

        defaults = {
            "desc": parser.get_key_unicode(data, "desc"),
            "contrib_desc": parser.get_key_unicode(data, "contrib_desc"),
        }

        ct, created = models.ContributionType.objects.get_or_create(
            source=source,
            name=parser.get_key_string(data, "name"),
            defaults=defaults)
        if not created:
            ct.desc = defaults["desc"]
            ct.contrib_desc = defaults["contrib_desc"]
            ct.save()

        return ct

    def get_source(self, data):
        """
        Validate and import one source from a dict
        """
        if not isinstance(data, dict):
            raise parser.Fail(400, "source data is not an object")

        defaults = {
            "desc": parser.get_key_unicode(data, "desc"),
            "url": parser.get_key_unicode(data, "url", empty=True),
            "auth_token": parser.get_key_string(data, "auth_token"),
        }

        ctypes = parser.get_key_sequence_or_object(data, "contribution_types")

        s, created = models.Source.objects.get_or_create(
            name=parser.get_key_string(data, "name"),
            defaults=defaults)
        if not created:
            s.desc = defaults["desc"]
            s.url = defaults["url"]
            s.auth_token = defaults["auth_token"]
            s.save()

        old_ctypes = {ct.pk: ct for ct in s.contribution_types.all()}

        for c in ctypes:
            ct = self.get_contribution_type(s, c)
            old_ctypes.pop(ct.pk, None)

        # Delete old ones
        for ct in old_ctypes.items():
            ct.delete()

    def import_sources(self, data):
        """
        Validate and import sources from a list of exported sources
        """
        try:
            if not isinstance(data, (list, tuple)):
                raise parser.Fail(400, "Sources data is not a list")

            for d in data:
                self.get_source(d)

            return True
        except parser.Fail as e:
            print(e)
            return False

class DataImporterV1:
    """
    Import exported data
    """
    def import_data(self, data):
        models.Source.import_json(data["sources"])
        log.info(f"Imported {len(data['sources'])} sources")

        self.encode_contribution_urls(data["contributions"])

        sers = {
                "users": serializers.UserSerializer,
                "identifiers": serializers.IdentifierSerializer,
                "identities": serializers.IdentitySerializer,
                "contributions": serializers.ContributionSerializer
                }
        for key, serializer in sers.items():
            s = serializer(data=data[key], many=True)
            if s.is_valid():
                s.save()
                log.info(f"Imported {len(data[key])} {key}")
            else:
                raise ValueError(f"{key} validation failed: {s.errors}")

        # Populate the aggregated tables
        models.AggregatedPersonContribution.recompute()
        models.AggregatedSource.recompute()
        models.AggregatedPerson.recompute()

    def encode_contribution_urls(self, contributions):
        for cont in contributions:
            if cont["url"]:
                cont["url"] = requote_uri(cont["url"])
