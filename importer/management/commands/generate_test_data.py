from django.core.management.base import BaseCommand
from contributors import models as cmodels
from signon.models import Identity
import logging
from datetime import datetime
from django.db.utils import IntegrityError

log = logging.getLogger(__name__)

def create_contributions(contribution_type, identifier):
    try:
        cmodels.Contribution.objects.create(type=contribution_type,
                identifier=identifier)
    except IntegrityError:
        # The contribution already exists.
        pass

def create_user(username, full_name, hidden=False):
    user, _ =  cmodels.User.objects.update_or_create(username=username,
            full_name=full_name, is_dd=True, is_staff=True, hidden=hidden)
    return user

def create_identity(person, issuer, subject, fullname, username=""):
    identity = Identity(person=person, issuer=issuer, subject=subject,
            fullname=fullname, username=username)
    try:
        identity.save(audit_skip=True)
    except IntegrityError as e:
        # The identity already exists
        pass

class Command(BaseCommand):
    help = 'Generate dummy data for testing'

    def handle(self, *args, **opts):
        # user 1
        user_1 = create_user("jsmith", "John Smith")

        create_identity(person=user_1, issuer="debsso",
                subject="jsmith@users.alioth.debian.org", fullname="John Smith")

        identifier_1, _ = cmodels.Identifier.objects.update_or_create(type="email",
                name="jsmith@debian.org", user=user_1)

        for name in "upload", "committer", "security-tracker", "announcements editor", "speaker":
            create_contributions(cmodels.ContributionType.objects.get(name=name),
                    identifier_1)

        # user 2
        user_2 = create_user("amunnelly", "Anika Munnelly")

        create_identity(person=user_2, issuer="debsso",
                subject="amunnelly@debian.org", fullname="Anika Munnelly")

        identifier_2, _ = cmodels.Identifier.objects.update_or_create(type="email",
                name="amunnelly@debian.org", user=user_2)

        for name in "bug-submission", "subber", "contributor":
            create_contributions(cmodels.ContributionType.objects.get(name=name),
                    identifier_2)

        # user 3
        user_3 = create_user("dpalmeri", "Deon Palmeri")

        create_identity(person=user_3, issuer="salsa", subject="dpalmeri@debian.org",
                fullname="Deon Palmeri", username="dpalmeri")

        identifier_3, _ = cmodels.Identifier.objects.update_or_create(type="email",
                name="dpalmeri@debian.org", user=user_3)

        for name in "committer", "developer", "post", "debian-installer-translator":
            create_contributions(cmodels.ContributionType.objects.get(name=name),
                    identifier_3)

        cmodels.UserSourceSettings.objects.update_or_create(user=user_3,
                source=cmodels.ContributionType.objects.get(name="debian-installer-translator").source,
                hidden=True)

        # user 4
        user_4 = create_user("adeakin", "Austin Deakin", hidden=True)

        create_identity(person=user_4, issuer="debsso",
                subject="adeakin@users.alioth.debian.org", fullname="Austin Deakin")

        identifier_4, _ = cmodels.Identifier.objects.update_or_create(type="email",
                name="adeakin@debian.org", user=user_4, hidden=True)

        for name in "upload", "committer", "security-tracker", "wml-translator", "debian-installer-reviewer":
            create_contributions(cmodels.ContributionType.objects.get(name=name),
                    identifier_4)

        # Populate the aggregated tables
        cmodels.AggregatedPersonContribution.recompute()
        cmodels.AggregatedSource.recompute()
        cmodels.AggregatedPerson.recompute()
