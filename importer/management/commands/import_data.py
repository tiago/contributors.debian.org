from django.core.management.base import BaseCommand
import sys
import logging
import json
import logging
from importer.importer import DataImporterV1

log = logging.getLogger(__name__)

class Command(BaseCommand):
    help = "Import exported public data"

    def add_arguments(self, parser):
        parser.add_argument("file", type=str, help="Exported JSON file path")
        parser.add_argument(
                '--quiet',
                action='store_true',
                dest='quiet',
                default=None,
                help='Disable progress reporting'
                )

    def handle(self, *args, **opts):
        FORMAT = "%(asctime)-15s %(levelname)s %(message)s"
        if opts["quiet"]:
            logging.basicConfig(level=logging.WARNING, stream=sys.stderr, format=FORMAT)
        else:
            logging.basicConfig(level=logging.INFO, stream=sys.stderr, format=FORMAT)

        with open(opts["file"]) as fd:
            json_data = json.load(fd)

            if json_data["version"] == 1:
                imp = DataImporterV1()
            else:
                raise NotImplementedError("Imported data version is not yet supported.")

            imp.import_data(json_data)

        log.info("Import is complete.")
