from django.urls import path
from . import views

urlpatterns = [
    path('logs', views.Logs.as_view(), name="importer_logs"),
    path('status', views.Status.as_view(), name="importer_status"),
    path('status/<str:sname>', views.SourceStatus.as_view(), name="importer_source_status"),
    path('submission/<int:pk>', views.ShowSubmission.as_view(), name="importer_submission"),
]
