from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View, TemplateView
from django.utils import timezone
from django import http
from dc.mixins import VisitorMixin
from .importer import Importer
from importer.models import Result, Submission, SubmissionLog
from importer.exporter import export_public_data
from sources.views import SourceMixin
import contributors.models as cmodels
import datetime
import json
import gc


class CSRFExemptMixin:
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(CSRFExemptMixin, self).dispatch(*args, **kwargs)


class PostSourceView(CSRFExemptMixin, View):
    def get(self, request, *args, **kw):
        res = http.HttpResponse(content_type="application/json")
        json.dump({}, res, indent=2)
        return res

    def post(self, request, *args, **kw):
        # Import the data
        importer = Importer()
        receive_log = importer.enqueue_request(request)
        gc.collect()
        return receive_log.to_response(request)


class TestPostSourceView(CSRFExemptMixin, View):
    def get(self, request, *args, **kw):
        res = http.HttpResponse(content_type="application/json")
        json.dump({}, res, indent=2)
        return res

    def post(self, request, *args, **kw):
        # Import the data
        importer = Importer()
        receive_log = importer.test_enqueue_request(request)
        gc.collect()
        return receive_log.to_response(request)


class ExportSources(VisitorMixin, View):
    def get(self, request, *args, **kw):
        if not self.request.user.is_authenticated or not self.request.user.is_superuser:
            data = cmodels.Source.export_json()
        else:
            data = cmodels.Source.export_json(with_tokens=True)

        res = http.HttpResponse(content_type="application/json")
        json.dump(data, res, indent=2)
        return res


class ExportDB(VisitorMixin, View):
    require_visitor = "dd"

    def get(self, request, *args, **kw):
        data = export_public_data()
        res = http.HttpResponse(content_type="application/json")
        res["Content-Disposition"] = "attachment; filename=contributors-public.json"
        json.dump(data, res, indent=2)
        return res


class Logs(TemplateView):
    template_name = "importer/logs.html"

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        days = self.request.GET.get("days", 30)
        ctx["log"] = Result.objects.filter(
                timestamp__gt=timezone.now() - datetime.timedelta(days=days)).order_by("-timestamp")
        ctx["days"] = days
        return ctx


class ShowSubmission(VisitorMixin, TemplateView):
    require_visitor = "user"
    template_name = "importer/submission.html"

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        submission = Submission.objects.get(pk=self.kwargs["pk"])
        ctx["submission"] = submission
        ctx["is_admin"] = submission.source.can_admin(self.request.user)
        return ctx


class Status(VisitorMixin, TemplateView):
    require_visitor = "user"
    template_name = "importer/status.html"

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        ctx["pending"] = SubmissionLog.objects.filter(
                submission__completed__isnull=True, operation="receive"
                ).order_by("submission__received").select_related()
        ctx["failed_receive"] = SubmissionLog.objects.exclude(result_code=200).filter(
                operation="receive").order_by("-submission__received").select_related()[:10]
        ctx["failed_import"] = SubmissionLog.objects.exclude(result_code=200).filter(
                operation="import").order_by("-submission__received").select_related()[:10]
        ctx["success_latest"] = SubmissionLog.objects.filter(result_code=200).filter(
                operation="import").order_by("-submission__received").select_related()[:10]
        return ctx


class SourceStatus(SourceMixin, TemplateView):
    require_visitor = "user"
    template_name = "importer/source_status.html"

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        threshold = timezone.now() - datetime.timedelta(days=90)
        ctx["logs"] = SubmissionLog.objects.filter(
                submission__source=self.source, submission__received__gte=threshold).order_by("-since").select_related()
        return ctx
