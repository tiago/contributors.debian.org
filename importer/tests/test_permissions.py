from django.test import TestCase
from django.urls import reverse
from django.utils import timezone
from importer import models as imodels
from dc.unittest import NamedObjects, SourceFixtureMixin
import datetime
import json


class ImporterTestCase(SourceFixtureMixin, TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.add_named_objects(
            submissions=NamedObjects(imodels.Submission),
            submission_logs=NamedObjects(imodels.SubmissionLog),
        )
        cls.submissions.create(
            "test",
            source=cls.sources.test,
            method="replace",
            relpath="/dev/null",
            received=datetime.datetime(2019, 3, 21, 12, 0, tzinfo=timezone.utc),
            completed=datetime.datetime(2019, 3, 21, 13, 0, tzinfo=timezone.utc))

        receive_stats = imodels.ReceiveStats(request=None, records_parsed=123)
        cls.submission_logs.create(
            "receive",
            submission=cls.submissions.test,
            operation="receive",
            since=100,
            until=110,
            result_code=200,
            stats=json.dumps(receive_stats.to_jsonable()))

        import_stats = imodels.ImportStats(
            errors=["Test error message"],
            identifiers_skipped=23,
            contributions_processed=100,
            contributions_created=10,
            contributions_updated=90,
            contributions_skipped=23,
            records_parsed=123,
            records_skipped=23)
        cls.submission_logs.create(
            "import",
            submission=cls.submissions.test,
            operation="import",
            since=150,
            until=170,
            result_code=400,
            stats=json.dumps(import_stats.to_jsonable()))

    @classmethod
    def __add_extra_tests__(cls):
        for user in "dd", "dd1", "alioth", "alioth1":
            cls._add_method(cls._test_importer_status_success, user)
            cls._add_method(cls._test_importer_source_status_success, user)
            cls._add_method(cls._test_importer_submission_success_without_sensitive_data, user)

        cls._add_method(cls._test_importer_status_success, "admin")
        cls._add_method(cls._test_importer_source_status_success, "admin")
        cls._add_method(cls._test_importer_submission_success_with_sensitive_data, "admin")

        cls._add_method(cls._test_importer_status_forbidden, None)
        cls._add_method(cls._test_importer_source_status_forbidden, None)
        cls._add_method(cls._test_importer_submission_forbidden, None)

    def _test_importer_status_success(self, user):
        client = self.make_test_client(user)
        response = client.get(reverse("importer_status"))
        self.assertContains(response, "Importer status")
        self.assertNotContains(response, "Test error message")

    def _test_importer_status_forbidden(self, user):
        client = self.make_test_client(user)
        response = client.get(reverse("importer_status"))
        self.assertPermissionDenied(response)

    def _test_importer_source_status_success(self, user):
        client = self.make_test_client(user)
        response = client.get(reverse("importer_source_status", args=["test"]))
        self.assertContains(response, "Submissions for test")
        self.assertNotContains(response, "Test error message")

    def _test_importer_source_status_forbidden(self, user):
        client = self.make_test_client(user)
        response = client.get(reverse("importer_source_status", args=["test"]))
        self.assertPermissionDenied(response)

    def _test_importer_submission_success_without_sensitive_data(self, user):
        client = self.make_test_client(user)
        response = client.get(reverse("importer_submission", args=[self.submissions.test.pk]))
        self.assertContains(response, "Submission status")
        self.assertNotContains(response, "Test error message")

    def _test_importer_submission_success_with_sensitive_data(self, user):
        client = self.make_test_client(user)
        response = client.get(reverse("importer_submission", args=[self.submissions.test.pk]))
        self.assertContains(response, "Submission status")
        self.assertContains(response, "Test error message")

    def _test_importer_submission_forbidden(self, user):
        client = self.make_test_client(user)
        response = client.get(reverse("importer_submission", args=[self.submissions.test.pk]))
        self.assertPermissionDenied(response)
