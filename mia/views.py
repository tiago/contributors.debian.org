from django.utils.translation import gettext as _
from django.views.generic import View
from django import http
from dc.mixins import VisitorMixin
from contributors.models import Contribution
from collections import defaultdict
import json


class ByFingerprint(VisitorMixin, View):
    def get(self, request, *args, **kw):
        by_type = defaultdict(dict)

        contribs = Contribution.objects.filter(
                identifier__type="fpr",
                type__source__name="ftp.debian.org")
        for c in contribs.select_related("type", "type__source", "identifier"):
            source = c.type.source.name + "/" + c.type.name
            by_type[source][c.identifier.name] = c.until.strftime("%Y-%m-%d")

        contribs = Contribution.objects.filter(
                identifier__type="login",
                type__source__name="vote.debian.org")
        for c in contribs.select_related("type", "type__source", "identifier"):
            source = c.type.source.name + "/" + c.type.name
            by_type[source][c.identifier.name] = c.until.strftime("%Y-%m-%d")

        res = http.HttpResponse(content_type="application/json")
        json.dump(by_type, res, indent=2)
        return res
