# Copyright © 2020 Truelite S.r.l
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import re
from django.contrib.auth import get_user_model
from django.urls import reverse
from django.test import Client
from model_bakery import baker


class NamedObjects(dict):
    """
    Container for fixture model objects.
    """
    def __init__(self, model, skip_delete_all=False, **defaults):
        super(NamedObjects, self).__init__()
        self._model = model
        self._defaults = defaults
        self._skip_delete_all = skip_delete_all
        self._pks = None

    def __getitem__(self, key):
        """
        Dict that only looks things up if they are strings, otherwise just return key.

        This allows to use __getitem__ with already resolved objects, just to have
        functions that can take either objects or their fixture names.
        """
        if not isinstance(key, str):
            return key
        return super(NamedObjects, self).__getitem__(key)

    def __getattr__(self, key):
        """
        Make dict elements also appear as class members
        """
        res = self.get(key, None)
        if res is not None:
            return res
        raise AttributeError("member {} not found".format(key))

    def _update_kwargs_with_defaults(self, _name, kw):
        """
        Update the kw dict with defaults from self._defaults.

        If self._defaults for an argument is a string, then calls .format() on
        it passing _name and self._defaults as format arguments.
        """
        for k, v in self._defaults.items():
            if isinstance(v, str):
                kw.setdefault(k, v.format(_name=_name, **self._defaults))
            elif hasattr(v, "__call__"):
                kw.setdefault(k, v(_name, **self._defaults))
            else:
                kw.setdefault(k, v)

    def create(self, _name, **kw):
        self._update_kwargs_with_defaults(_name, kw)
        self[_name] = o = baker.make(self._model, **kw)
        self.pks[_name] = o.pk
        return o

    def refresh(self):
        """
        Reload all the objects from the database.

        This is needed because although Django's TestCase rolls back the
        database after a test, the data stored in memory in the objects stored
        in NamedObjects repositories is not automatically refreshed.
        """
        for name, o in list(self.items()):
            try:
                self[name].refresh_from_db()
            except self._model.DoesNotExist:
                del self[name]

    def restore(self):
        """
        Restore the object repository to its pristine state after a transaction
        rollback
        """
        if self._pks is None:
            # First, run, save pks
            self._pks = {name: o.pk for name, o in self.items()}
        else:
            self.clear()
            for name, pk in self._pks.items():
                self[name] = self._model.objects.get(pk=pk)

    def delete_all(self):
        """
        Call delete() on all model objects registered in this dict.

        This can be used in methods like tearDownClass to remove objects common
        to all tests.
        """
        if self._skip_delete_all:
            return
        for o in self.values():
            if not o.pk:
                continue
            o.delete()


class TestUsers(NamedObjects):
    def __init__(self, **defaults):
        defaults.setdefault("first_name", "{_name}first")
        defaults.setdefault("last_name", "{_name}last")
        defaults.setdefault("email", "{_name}@example.com")
        defaults.setdefault("password", "{_name}@example.com")
        super().__init__(get_user_model(), **defaults)

    def create(self, _name, **kw):
        self._update_kwargs_with_defaults(_name, kw)
        is_superuser = kw.pop("is_superuser", False)
        if is_superuser:
            self[_name] = o = self._model.objects.create_superuser(**kw)
        else:
            self[_name] = o = self._model.objects.create_user(**kw)
        return o


class TestMeta(type):
    def __new__(cls, name, bases, attrs):
        res = super(TestMeta, cls).__new__(cls, name, bases, attrs)
        if hasattr(res, "__add_extra_tests__"):
            res.__add_extra_tests__()
        return res


class TestBase(metaclass=TestMeta):
    @classmethod
    def _add_method(cls, meth, *args, **kw):
        """
        Add a test method, made of the given method called with the given args
        and kwargs.

        The method name and args are used to built the test method name, the
        kwargs are not: make sure you use the args to make the test case
        unique, and the kwargs for things you do not want to appear in the test
        name, like the expected test results for those args.
        """
        name = re.sub(r"[^0-9A-Za-z_]", "_", "{}_{}".format(meth.__name__.lstrip("_"), "_".join(str(x) for x in args)))
        setattr(cls, name, lambda self: meth(self, *args, **kw))

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls._object_repos = []

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()
        for r in cls._object_repos[::-1]:
            r.delete_all()

    def setUp(self):
        super().setUp()
        for r in self._object_repos:
            r.restore()

    @classmethod
    def add_named_objects(cls, **kw):
        for name, repo in kw.items():
            cls._object_repos.append(repo)
            setattr(cls, name, repo)

    def make_test_client(self, user, **kw):
        """
        Instantiate a test client, logging in the given user.
        """
        user = self.users[user]
        client = Client(**kw)
        if user is not None:
            client.force_login(user)
        client.visitor = user
        return client

    def assertPermissionDenied(self, response):
        if response.status_code == 403:
            pass
        elif response.status_code == 302:
            self.assertRedirectMatches(response, reverse("signon:login"))
        else:
            self.fail(
                "response has status code {} instead of a 403 Forbidden or a 302 Redirect".format(response.status_code))

    def assertRedirectMatches(self, response, target):
        if response.status_code != 302:
            self.fail("response has status code {} instead of a Redirect".format(response.status_code))
        if target and not re.search(target, response["Location"]):
            self.fail("response redirects to {} which does not match {}".format(response["Location"], target))

    def assertFormErrorMatches(self, response, form_name, field_name, regex):
        form = response.context[form_name]
        errors = form.errors
        if not errors:
            self.fail("Form {} has no errors".format(form_name))
        if field_name not in errors:
            self.fail("Form {} has no errors in field {}".format(form_name, field_name))
        match = re.compile(regex)
        for errmsg in errors[field_name]:
            if match.search(errmsg):
                return
        self.fail("{} dit not match any in {}".format(regex, repr(errors)))


class UserFixtureMixin(TestBase):
    test_users_cls = TestUsers

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.add_named_objects(users=cls.test_users_cls())

    def set_perms(self, user, perms=[]):
        from django.contrib.auth.models import Permission
        if user is None:
            return
        self.users[user].user_permissions.clear()
        for perm in perms:
            if "." in perm:
                app_label, codename = perm.split(".", 1)
                self.users[user].user_permissions.add(
                        Permission.objects.get(codename=codename, content_type__app_label=app_label))
            else:
                self.users[user].user_permissions.add(Permission.objects.get(codename=perm))
