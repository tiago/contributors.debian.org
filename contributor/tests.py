from __future__ import annotations
from django.test import TestCase
from django.urls import reverse
from dc.unittest import SourceFixtureMixin, NamedObjects
import re


class ContributorView(SourceFixtureMixin, TestCase):
    @classmethod
    def __add_extra_tests__(cls):
        # Supervisor can see full details on all
        for user in "admin", "dd", "dd1", "alioth", "alioth1":
            cls._add_method(cls._test_sees_all, "admin", user)
            cls._add_method(cls._test_sees_hidden, "admin", user)

        # Others can only see full details on self
        for u in "admin", "dd", "dd1", "alioth", "alioth1":
            for vu in "dd", "dd1", "alioth", "alioth1":
                if u == vu:
                    cls._add_method(cls._test_sees_all, vu, u)
                    cls._add_method(cls._test_sees_hidden, vu, u)
                else:
                    cls._add_method(cls._test_sees_redacted, vu, u)
                    cls._add_method(cls._test_does_not_see_hidden, vu, u)

        # Check that each user can see their settings
        for u in "admin", "dd", "alioth":
            cls._add_method(cls._test_user_settings, u)
            cls._add_method(cls._test_ident_settings, u)

    @classmethod
    def setUpClass(cls):
        from signon.models import Identity
        super().setUpClass()
        cls.add_named_objects(
            identities=NamedObjects(Identity),
        )

    def _test_sees_all(self, visitor, visited):
        client = self.make_test_client(visitor)
        response = client.get(reverse('contributor:detail', kwargs={"name": self.users[visited].username}))
        self.assertEqual(response.status_code, 200)
        self.assertIn("contributions", response.context)
        self.assertIn("user_log", response.context)
        self.assertIn("identifiers", response.context)
        self.assertIn("source_settings", response.context)
        self.assertIn("hide_user", response.context)

    def _test_sees_redacted(self, visitor, visited):
        client = self.make_test_client(visitor)
        visited = self.users[visited]
        response = client.get(reverse('contributor:detail', kwargs={"name": visited.username}))
        self.assertEqual(response.status_code, 200)
        self.assertIn("contributions", response.context)
        self.assertNotIn("user_log", response.context)
        self.assertNotIn("identifiers", response.context)
        self.assertNotIn("source_settings", response.context)
        self.assertNotIn("hide_user", response.context)

    def _test_user_settings(self, user):
        client = self.make_test_client(user)

        self.users[user].hidden = False
        self.users[user].save()
        response = client.get(reverse('contributor:detail', kwargs={"name": self.users[user].username}))
        self.assertEqual(response.status_code, 200)
        checkbox = re.findall(r'<input type="checkbox" name="hide_user" [^>]+>', response.content.decode())[0]
        self.assertNotIn("checked", checkbox)

        response = client.post(reverse('contributor:save_settings', kwargs={"name": self.users[user].username}), data={
            "hide_user": "on",
        })
        self.assertRedirectMatches(response, self.users[user].get_absolute_url())

        self.users[user].refresh_from_db()
        self.assertTrue(self.users[user].hidden)

        response = client.get(reverse('contributor:detail', kwargs={"name": self.users[user].username}))
        self.assertEqual(response.status_code, 200)
        checkbox = re.findall(r'<input type="checkbox" name="hide_user" [^>]+>', response.content.decode())[0]
        self.assertIn("checked", checkbox)

    def _test_ident_settings(self, user):
        client = self.make_test_client(user)

        ident = self.idents[f"{user}_home"]

        ident.hidden = False
        ident.save()

        response = client.get(reverse('contributor:detail', kwargs={"name": self.users[user].username}))
        self.assertEqual(response.status_code, 200)
        checkbox = re.findall(fr'<input type="checkbox" name="hide_{ident.pk}" [^>]+>', response.content.decode())[0]
        self.assertNotIn("checked", checkbox)

        response = client.post(reverse('contributor:save_settings', kwargs={"name": self.users[user].username}), data={
            f"hide_{ident.pk}": "on",
        })
        self.assertRedirectMatches(response, self.users[user].get_absolute_url())

        ident.refresh_from_db()
        self.assertTrue(ident.hidden)

        response = client.get(reverse('contributor:detail', kwargs={"name": self.users[user].username}))
        self.assertEqual(response.status_code, 200)
        checkbox = re.findall(fr'<input type="checkbox" name="hide_{ident.pk}" [^>]+>', response.content.decode())[0]
        self.assertIn("checked", checkbox)

    def _test_sees_hidden(self, visitor, visited):
        self.users[visited].hidden = True
        self.users[visited].save()
        self._test_sees_all(visitor, visited)

    def _test_does_not_see_hidden(self, visitor, visited):
        self.users[visited].hidden = True
        self.users[visited].save()
        client = self.make_test_client(visitor)
        response = client.get(reverse('contributor:detail', kwargs={"name": self.users[visited].username}))
        self.assertEqual(response.status_code, 404)

    def test_lookup(self):
        self.identities.create(
                "alioth_salsa", person=self.users.alioth,
                issuer="salsa", subject="2", username="alioth", audit_skip=True)
        self.identities.create(
                "alioth_debssso", person=self.users.alioth,
                issuer="debsso", subject="alioth-guest@users.alioth.debian.org",
                username="alioth-guest@users.alioth.debian.org", audit_skip=True)
        self.identities.create(
                "alioth_debssso_debian", person=self.users.alioth,
                issuer="debsso", subject="alioth@debian.org",
                username="alioth@debian.org", audit_skip=True)
        self.users.alioth.username = "alioth"
        self.users.alioth.save()

        client = self.make_test_client(None)

        response = client.get(reverse('contributor:detail', kwargs={"name": "alioth"}))
        self.assertEqual(response.status_code, 200)
        response = client.get(reverse('contributor:detail', kwargs={"name": "alioth@salsa"}))
        self.assertEqual(response.status_code, 200)
        response = client.get(reverse('contributor:detail', kwargs={"name": "alioth-guest@alioth"}))
        self.assertEqual(response.status_code, 200)
        response = client.get(reverse('contributor:detail', kwargs={"name": "alioth@debian"}))
        self.assertEqual(response.status_code, 200)
        response = client.get(reverse('contributor:detail', kwargs={"name": "alioth@debian.org"}))
        self.assertEqual(response.status_code, 404)
        response = client.get(reverse('contributor:detail', kwargs={"name": "alioth-guest@users.alioth.debian.org"}))
        self.assertEqual(response.status_code, 301)
        self.assertEqual(response.url, "/contributor/alioth/")
        response = client.get(reverse('contributor:detail', kwargs={"name": "alioth@example.org"}))
        self.assertEqual(response.status_code, 301)
        self.assertEqual(response.url, "/contributor/alioth/")
